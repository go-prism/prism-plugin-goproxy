package api

import (
	"github.com/gorilla/mux"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"net/http"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	// VersionSuffix matches suffixes with /v0 or /v1
	// which are considered illegal by Go.
	VersionSuffix = regexp.MustCompile(`/v([0-1]|)$`)
)

type Matcher struct {
	srv    Modules
	prefix string
}

func (m *Matcher) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "matcher_serveHTTP")
	defer span.End()
	uri := strings.TrimPrefix(r.URL.Path, m.prefix)
	bits := strings.Split(uri, "/@v/")
	if len(bits) != 2 {
		http.NotFound(w, r)
		return
	}
	ext := filepath.Ext(bits[1])
	// get the module name
	moduleName := VersionSuffix.ReplaceAllString(bits[0], "")
	// get the module version and trim the extension
	moduleVersion := strings.TrimSuffix(bits[1], ext)
	span.SetAttributes(
		attribute.String("ext", ext),
		attribute.String("name", moduleName),
		attribute.String("version", moduleVersion),
	)
	// if this is a list function, do that
	if moduleVersion == "list" {
		m.srv.ListVersions(w, mux.SetURLVars(r.Clone(ctx), map[string]string{
			"module": moduleName,
		}))
		return
	}
	// insert mux variables
	req := mux.SetURLVars(r.Clone(ctx), map[string]string{
		"module":  moduleName,
		"version": moduleVersion,
	})
	// figure out which handler to call
	switch ext {
	case ".info":
		m.srv.GetVersion(w, req)
	case ".mod":
		m.srv.GetModuleInfo(w, req)
	case ".zip":
		m.srv.GetModule(w, req)
	default:
		http.NotFound(w, req)
	}
}
