package api

import (
	"context"
	"errors"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"golang.org/x/mod/module"
	"io"
	"time"
)

var (
	ErrNotFound         = errors.New("404 not found, it may not be a valid git repo")
	ErrPathMismatch     = errors.New("asked for a module path but received a different one")
	ErrMalformedCommand = errors.New("a go command must have at least 1 argument")
)

// Info contains metadata about a specific version of
// a module.
//
// https://golang.org/ref/mod#goproxy-protocol
type Info struct {
	Version string    `json:"version"`
	Time    time.Time `json:"time"`
}

type BaseModService interface {
	ListVersions(ctx context.Context, moduleName string) ([]*Info, error)
	GetVersion(ctx context.Context, version *module.Version) (*Info, error)
	GetModuleInfo(ctx context.Context, w io.Writer, version *module.Version) error
	GetModule(ctx context.Context, w io.Writer, version *module.Version) error
	GetLatestVersion(ctx context.Context, moduleName string) (*Info, error)
}

type Clients struct {
	ReactorClient reactor.ReactorClient
	ModulesClient api.ModulesClient
}

type downloadInfo struct {
	Path    string
	Version string
	Info    string
	GoMod   string
	Zip     string
}
