package api

import (
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testModules struct{}

func (*testModules) ListVersions(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusCreated)
}
func (*testModules) GetVersion(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusAccepted)
}
func (*testModules) GetModuleInfo(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusNonAuthoritativeInfo)
}
func (*testModules) GetModule(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

type testModules2 struct {
	f func(w http.ResponseWriter, r *http.Request)
}

func (t *testModules2) ListVersions(w http.ResponseWriter, r *http.Request) {
	t.f(w, r)
}
func (t *testModules2) GetVersion(w http.ResponseWriter, r *http.Request) {
	t.f(w, r)
}
func (t *testModules2) GetModuleInfo(w http.ResponseWriter, r *http.Request) {
	t.f(w, r)
}
func (t *testModules2) GetModule(w http.ResponseWriter, r *http.Request) {
	t.f(w, r)
}

var dummy = &testModules{}

func TestMatcher_ServeHTTP(t *testing.T) {
	var cases = []struct {
		name    string
		target  string
		code    int
		module  string
		version string
	}{
		{
			"list is called",
			"/plugin/-/golang.org/x/text/@v/list",
			http.StatusCreated,
			"golang.org/x/text",
			"",
		},
		{
			"version info is called",
			"/plugin/-/golang.org/x/text/@v/v1.2.3.info",
			http.StatusAccepted,
			"golang.org/x/text",
			"v1.2.3",
		},
		{
			"module info is called",
			"/plugin/-/golang.org/x/text/@v/v1.2.3.mod",
			http.StatusNonAuthoritativeInfo,
			"golang.org/x/text",
			"v1.2.3",
		},
		{
			"module is called",
			"/plugin/-/golang.org/x/text/@v/v1.2.3.zip",
			http.StatusNoContent,
			"golang.org/x/text",
			"v1.2.3",
		},
		{
			"module with majorPrefix is called",
			"/plugin/-/github.com/mediocregopher/radix/v3/@v/v3.3.0.zip",
			http.StatusNoContent,
			"github.com/mediocregopher/radix/v3",
			"v3.3.0",
		},
		{
			"latest is (currently) not found",
			"/plugin/-/golang.org/x/text/@latest",
			http.StatusNotFound,
			"",
			"",
		},
		{
			"incorrect syntax is not found",
			"/plugin/-/golang.org/x/text/@d/list",
			http.StatusNotFound,
			"",
			"",
		},
		{
			"sub-version is handled correctly",
			"/plugin/-/github.com/unleash/unleash-client-go/v3/@v/v3.1.1.mod",
			http.StatusNonAuthoritativeInfo,
			"github.com/unleash/unleash-client-go/v3",
			"v3.1.1",
		},
		{
			"sub-version is handled correctly",
			"/plugin/-/github.com/unleash/unleash-client-go/v1/@v/v1.1.1.mod",
			http.StatusNonAuthoritativeInfo,
			"github.com/unleash/unleash-client-go",
			"v1.1.1",
		},
	}

	var m = &Matcher{
		srv:    dummy,
		prefix: "/plugin/-/",
	}
	// check that the appropriate function is called
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			m.ServeHTTP(w, httptest.NewRequest(http.MethodGet, tt.target, nil))
			res := w.Result()
			defer res.Body.Close()

			assert.EqualValues(t, tt.code, res.StatusCode)
		})
	}

	var m2 *Matcher

	// check that module name/version is parsed
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			m2 = &Matcher{
				srv: &testModules2{func(w http.ResponseWriter, r *http.Request) {
					vars := mux.Vars(r)
					mn := vars["module"]
					mv := vars["version"]
					assert.EqualValues(t, tt.module, mn)
					assert.EqualValues(t, tt.version, mv)
				}},
				prefix: "/plugin/-/",
			}
			w := httptest.NewRecorder()
			m2.ServeHTTP(w, httptest.NewRequest(http.MethodGet, tt.target, nil))
		})
	}
}
