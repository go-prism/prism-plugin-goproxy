package api

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/go-prism/prism-plugin-goproxy/pkg/internal/metrics"
)

var (
	metricDownload = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: metrics.Namespace,
		Subsystem: metrics.Subsystem,
		Name:      "module_download_total",
	})
	metricDownloadErrs = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: metrics.Namespace,
		Subsystem: metrics.Subsystem,
		Name:      "module_download_errs_total",
	})
)
