package api

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	"go.opentelemetry.io/otel"
	"golang.org/x/mod/module"
	"net/http"
	"sort"
	"strings"

	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
)

type Modules interface {
	ListVersions(w http.ResponseWriter, r *http.Request)
	GetVersion(w http.ResponseWriter, r *http.Request)
	GetModuleInfo(w http.ResponseWriter, r *http.Request)
	GetModule(w http.ResponseWriter, r *http.Request)
}

type GoModules struct {
	svc BaseModService
}

func NewGoModules(svc BaseModService, r *mux.Router) *GoModules {
	api := new(GoModules)
	api.svc = svc

	m := &Matcher{
		srv:    api,
		prefix: "/plugin/-/",
	}
	r.Handle("/plugin/-/{path:.*}", m).
		Methods(http.MethodGet)

	return api
}

func (api *GoModules) returnError(w http.ResponseWriter, err error) bool {
	if err == nil {
		return false
	}
	// if the error is a 404, return a 404
	if errors.Is(err, ErrNotFound) {
		http.Error(w, err.Error(), http.StatusNotFound)
		return true
	} else if errors.Is(err, ErrPathMismatch) || errors.Is(err, ErrMalformedCommand) {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return true
	}
	// otherwise, throw a standard 500
	http.Error(w, err.Error(), http.StatusInternalServerError)
	return true
}

func (api *GoModules) ListVersions(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "api_modules_listVersions")
	defer span.End()
	moduleName := mux.Vars(r)["module"]
	if moduleName == "" {
		http.Error(w, "module name must be provided", http.StatusBadRequest)
		return
	}
	info, err := api.svc.ListVersions(ctx, moduleName)
	if err != nil {
		api.returnError(w, err)
		return
	}
	// go wants versions to be sorted descending
	// see #12
	sort.Slice(info, func(i, j int) bool {
		if info[i] == nil || info[j] == nil {
			log.WithContext(r.Context()).Debugf("skipping nil element during sort at position %d or %d", i, j)
			return false
		}
		return info[i].Version > info[j].Version
	})
	// write the tags into a one-per-line list
	var sb strings.Builder
	for _, i := range info {
		sb.WriteString(i.Version)
		sb.WriteString("\n")
	}
	_, _ = w.Write([]byte(sb.String()))
}

func (api *GoModules) GetVersion(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "api_modules_getVersion")
	defer span.End()
	moduleName := mux.Vars(r)["module"]
	if moduleName == "" {
		http.Error(w, "module name must be provided", http.StatusBadRequest)
		return
	}
	versionName := mux.Vars(r)["version"]
	info, err := api.svc.GetVersion(ctx, &module.Version{
		Path:    moduleName,
		Version: versionName,
	})
	if err != nil {
		api.returnError(w, err)
		return
	}
	httputils.ReturnJSON(w, http.StatusOK, info)
}

func (api *GoModules) GetModuleInfo(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "api_modules_getModuleInfo")
	defer span.End()
	moduleName := mux.Vars(r)["module"]
	if moduleName == "" {
		http.Error(w, "module name must be provided", http.StatusBadRequest)
		return
	}
	versionName := mux.Vars(r)["version"]
	err := api.svc.GetModuleInfo(ctx, w, &module.Version{
		Path:    moduleName,
		Version: versionName,
	})
	if err != nil {
		api.returnError(w, err)
		return
	}
}

func (api *GoModules) GetModule(w http.ResponseWriter, r *http.Request) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(r.Context(), "api_modules_getModule")
	defer span.End()
	moduleName := mux.Vars(r)["module"]
	if moduleName == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	versionName := mux.Vars(r)["version"]
	err := api.svc.GetModule(ctx, w, &module.Version{
		Path:    moduleName,
		Version: versionName,
	})
	if err != nil {
		api.returnError(w, err)
		return
	}
	w.Header().Set(httputils.ContentType, "application/zip")
}
