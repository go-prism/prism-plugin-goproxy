package api

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/testutils"
	"testing"
)

func TestInterceptorModService_rewriteModule(t *testing.T) {
	var cases = []struct {
		name       string
		moduleName string
		expected   string
		ok         bool
	}{
		{
			"github.com is rewritten",
			"github.com/djcass44/go-tracer",
			"git.example.org/djcass44/go-tracer",
			true,
		},
		{
			"gitlab.com is ignored",
			"gitlab.com/go-prism/prism-api",
			"gitlab.com/go-prism/prism-api",
			false,
		},
		{
			"invalid url is ignored",
			"github.com\t/something",
			"github.com\t/something",
			false,
		},
	}

	svc := &InterceptorModService{
		rulesClient: &testutils.TestRulesClient{},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			actual, ok := svc.rewriteModule(context.TODO(), tt.moduleName)
			assert.EqualValues(t, tt.ok, ok)
			assert.EqualValues(t, tt.expected, actual)
		})
	}
}
