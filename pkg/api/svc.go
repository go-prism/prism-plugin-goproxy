package api

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/djcass44/go-utils/pkg/httputils"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"go.opentelemetry.io/otel"
	"golang.org/x/mod/module"
	"io"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

type DeferredModService struct {
	refractionName string
	clients        *Clients
	versionCache   map[string]time.Time
}

func NewDeferredModService(refractionName string, clients *Clients) *DeferredModService {
	svc := new(DeferredModService)
	svc.refractionName = refractionName
	svc.clients = clients
	svc.versionCache = map[string]time.Time{}

	log.Infof("initialising deferred mod service with refraction: '%s'", svc.refractionName)

	return svc
}

func (svc *DeferredModService) ListVersions(ctx context.Context, moduleName string) ([]*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_listVersions")
	defer span.End()
	log.WithContext(ctx).Infof("listing versions for module %s", moduleName)
	// escape the module name, so we can pass
	// it to go properly
	name, err := module.UnescapePath(moduleName)
	if err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to unescape module path: %s", moduleName)
		return nil, err
	}
	var versions struct {
		Path     string
		Versions []string
	}
	if err := svc.exec(ctx, &versions, "list", "-m", "-json", "-versions", name+"@latest"); err != nil {
		return nil, err
	}
	if versions.Path != name {
		log.WithError(ErrPathMismatch).WithContext(ctx).Errorf("asked for %s but got %s", name, versions.Path)
		return nil, ErrPathMismatch
	}
	log.WithContext(ctx).Debugf("located %d versions", len(versions.Versions))
	info := make([]*Info, len(versions.Versions))
	for i, v := range versions.Versions {
		info[i] = &Info{
			Version: v,
			Time:    time.Time{},
		}
	}
	return info, nil
}

func (svc *DeferredModService) GetVersion(ctx context.Context, version *module.Version) (*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_getVersion")
	defer span.End()
	// check if we can find cached data
	modv, err := svc.clients.ModulesClient.Get(utils.SubmitCtx(ctx), &api.GetModRequest{
		Name:    version.Path,
		Version: version.Version,
	})
	if err == nil {
		// try to parse the timestamp
		ts, err := time.Parse(time.RFC3339, modv.GetTimestamp())
		if err != nil {
			log.WithError(err).WithContext(ctx).Errorf("failed to parse timestamp: '%s'", modv.GetTimestamp())
			ts = time.Time{}
		}
		// return the cached info
		return &Info{
			Version: modv.GetVersionName(),
			Time:    ts,
		}, nil
	}
	var buf bytes.Buffer
	// fetch the data from the reactor, or download it if necessary
	if err := svc.getReactiveData(ctx, version, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.info", version.Path, version.Version),
		Method:     http.MethodGet,
		Headers:    map[string]string{},
	}, &buf); err != nil {
		return nil, err
	}
	// convert the json data into a struct
	var info Info
	err = json.Unmarshal(buf.Bytes(), &info)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to unmarshal json data")
		return nil, err
	}
	// save the data into the API
	log.WithContext(ctx).Debugf("attempting to cache version: %+v", info)
	if _, err := svc.clients.ModulesClient.Create(utils.SubmitCtx(ctx), &api.CreateModRequest{
		Name:      version.Path,
		Version:   info.Version,
		Timestamp: info.Time.Format(time.RFC3339),
	}); err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to cache version information")
	}
	return &info, nil
}

func (svc *DeferredModService) GetModuleInfo(ctx context.Context, w io.Writer, version *module.Version) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_getModuleInfo")
	defer span.End()
	// fetch the data from the reactor, or download it if necessary
	return svc.getReactiveData(ctx, version, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.mod", version.Path, version.Version),
		Method:     http.MethodGet,
		Headers:    map[string]string{},
	}, w)
}

func (svc *DeferredModService) GetModule(ctx context.Context, w io.Writer, version *module.Version) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_getModule")
	defer span.End()
	// fetch the data from the reactor, or download it if necessary
	return svc.getReactiveData(ctx, version, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.zip", version.Path, version.Version),
		Method:     http.MethodGet,
		Headers:    map[string]string{},
	}, w)
}

func (svc *DeferredModService) GetLatestVersion(ctx context.Context, moduleName string) (*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_getLatestVersion")
	defer span.End()
	return svc.GetVersion(ctx, &module.Version{
		Path:    moduleName,
		Version: "latest",
	})
}

func (svc *DeferredModService) getReactiveData(ctx context.Context, version *module.Version, req *reactor.GetObjectRequest, w io.Writer) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_getReactiveData")
	defer span.End()
	log.WithContext(ctx).Infof("attempting to retrieve file from the Reactor: '%s/%s'", req.GetRefraction(), req.GetPath())
	// check if the reactor has a copy
	stream, err := svc.clients.ReactorClient.GetObjectV2(utils.SubmitCtx(ctx), req)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("received error from Reactor")
	} else {
		var ok bool
		for {
			// read data from the Reactor
			resp, err := stream.Recv()
			if err != nil {
				// bail out on EOF
				if errors.Is(err, io.EOF) {
					ok = true
					break
				}
				log.WithError(err).WithContext(ctx).Error("received unexpected error streaming data from the Reactor")
				ok = false
				break
			}
			// if we get a non-ok response code, bail out
			if httputils.IsHTTPError(int(resp.GetCode())) {
				log.WithContext(ctx).Infof("received unacceptable code from Reactor: %d", resp.GetCode())
				ok = false
				break
			}
			// otherwise write the data into the response
			// todo count the number of bytes to make sure we're not responding with zero
			bw, err := w.Write(resp.GetContent())
			log.WithError(err).WithContext(ctx).Debugf("wrote %d bytes to response", bw)
			ok = true
		}
		// if we got data, we don't need to continue
		if ok {
			log.WithContext(ctx).Info("looks like we found cached data, no need to fetch it from the source")
			return nil
		}
	}
	info, err := svc.download(ctx, *version)
	if err != nil {
		return err
	}
	// upload all the files we just downloaded
	svc.upload(ctx, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.info", version.Path, version.Version),
		Method:     http.MethodPost,
		Headers:    map[string]string{},
	}, info.Info)
	svc.upload(ctx, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.mod", version.Path, version.Version),
		Method:     http.MethodPost,
		Headers:    map[string]string{},
	}, info.GoMod)
	svc.upload(ctx, &reactor.GetObjectRequest{
		Refraction: svc.refractionName,
		Path:       fmt.Sprintf("%s/@v/%s.zip", version.Path, version.Version),
		Method:     http.MethodPost,
		Headers:    map[string]string{},
	}, info.Zip)
	// figure out which file we need to stream back
	ext := filepath.Ext(req.GetPath())
	var path string
	switch ext {
	case ".info":
		path = info.Info
	case ".mod":
		path = info.GoMod
	case ".zip":
		path = info.Zip
	default:
		return nil
	}
	// crack the file open
	file, err := os.Open(path)
	if err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to open file: %s", path)
		return err
	}
	// copy it directly into the writer
	bw, err := io.Copy(w, file)
	log.WithError(err).Debugf("copied %d bytes from %s", bw, path)
	return err
}

func (svc *DeferredModService) download(ctx context.Context, version module.Version) (*downloadInfo, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_download")
	defer span.End()
	v, err := module.UnescapePath(version.Path)
	if err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to unescape module path: %s", version.Path)
		return nil, err
	}
	version.Path = v
	log.WithContext(ctx).Infof("downloading %s", version.String())
	var info downloadInfo
	if err := svc.exec(ctx, &info, "mod", "download", "-json", version.String()); err != nil {
		metricDownloadErrs.Inc()
		return nil, err
	}
	metricDownload.Inc()
	return &info, nil
}

func (svc *DeferredModService) upload(ctx context.Context, req *reactor.GetObjectRequest, path string) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_upload")
	defer span.End()
	log.WithContext(ctx).Infof("preparing to upload data to refraction '%s' at path: %s", req.GetRefraction(), req.GetPath())
	// read the file
	file, err := os.Open(path)
	if err != nil {
		log.WithError(err).WithContext(ctx).Errorf("failed to open file: %s", path)
		return
	}
	// open the stream
	writer, err := svc.clients.ReactorClient.PutObjectV2(utils.SubmitCtx(ctx))
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to initiate upload")
		return
	}
	// send the request
	err = writer.Send(&reactor.PutObjectV2Request{
		Msg: &reactor.PutObjectV2Request_Request{
			Request: req,
		},
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to send request information")
		return
	}
	log.WithContext(ctx).Info("successfully sent request information, preparing to stream content")
	// stream the file back over the rpc connection
	chunks := 0
	var buf = make([]byte, 1024)
	for {
		chunks++
		n, err := file.Read(buf)
		if err != nil || errors.Is(err, io.EOF) {
			log.WithError(err).WithContext(ctx).Error("failed to read chuck, or we've reached the end")
			break
		}
		if err := writer.Send(&reactor.PutObjectV2Request{
			Msg: &reactor.PutObjectV2Request_Content{
				Content: buf[:n],
			},
		}); err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to write chunk")
			return
		}
	}
	log.WithContext(ctx).Infof("successfully wrote data in %d chunks", chunks)
	_, err = writer.CloseAndRecv()
	log.WithError(err).WithContext(ctx).Info("closing stream to the Reactor")
}

func (svc *DeferredModService) exec(ctx context.Context, v interface{}, command ...string) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_deferred_exec")
	defer span.End()
	if len(command) < 1 {
		span.RecordError(ErrMalformedCommand)
		log.WithError(ErrMalformedCommand).WithContext(ctx).Error("command has no arguments and cannot be executed")
		return ErrMalformedCommand
	}
	log.WithContext(ctx).Debugf("executing go command: %+v", command)
	cmd := exec.CommandContext(ctx, "go", command...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	// run the command and catch any error
	if err := cmd.Run(); err != nil {
		span.RecordError(err)
		output := fmt.Sprintf("%s|%s", stderr.String(), stdout.String())
		log.WithError(err).WithContext(ctx).Errorf("go %+v:\n%s", command, output)
		return svc.parseCode(output)
	}
	// try to parse the response json
	if err := json.Unmarshal(stdout.Bytes(), v); err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Errorf("failed to read json from command: %+v", command)
		return err
	}
	return nil
}

// parseCode attempts to parse a http code from the
// golang cli error messages
func (*DeferredModService) parseCode(output string) error {
	if strings.Contains(output, "no matching versions for query") || strings.Contains(output, "unrecognized import path") {
		return ErrNotFound
	}
	return errors.New(output)
}
