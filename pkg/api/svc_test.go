package api

import (
	"bytes"
	"context"
	_ "embed"
	"fmt"
	"github.com/mholt/archiver/v3"
	uuid "github.com/satori/go.uuid"
	"github.com/vikyd/go-checksum/checksum"
	"golang.org/x/mod/module"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/testutils"
)

// interface guard
var _ BaseModService = &DeferredModService{}

var clients = &Clients{
	ReactorClient: &testutils.TestReactorClient{},
	ModulesClient: testutils.NewTestModuleClient(),
}

func TestMain(m *testing.M) {
	_ = os.Setenv("GOPROXY", "")
	os.Exit(m.Run())
}

func TestModService_ListVersions(t *testing.T) {
	var svc = NewDeferredModService("test", clients)
	_, err := svc.ListVersions(context.TODO(), "golang.org/x/text")
	assert.NoError(t, err)
}

func TestModService_GetVersion(t *testing.T) {
	var cases = []struct {
		name        string
		moduleName  string
		versionName string
		expected    string
	}{
		{
			"standard semantic resolves",
			"golang.org/x/text",
			"v0.3.2",
			"v0.3.2",
		},
		{
			"repeat is now cached",
			"golang.org/x/text",
			"v0.3.2",
			"v0.3.2",
		},
		{
			"pseudo-version resolves",
			"google.golang.org/genproto",
			"v0.0.0-20190307195333-5fe7a883aa19",
			"v0.0.0-20190307195333-5fe7a883aa19",
		},
		{
			"incompatible version resolves",
			"github.com/dgrijalva/jwt-go",
			"v3.2.0+incompatible",
			"v3.2.0+incompatible",
		},
		{
			"sub-module resolves",
			"golang.org/x/tools/gopls",
			"v0.7.0",
			"v0.7.0",
		},
		{
			"no-10-issue-0",
			"cloud.google.com/go/bigquery",
			"v1.18.0",
			"v1.18.0",
		},
		{
			"no-10-issue-1",
			"cloud.google.com/go/storage",
			"v1.6.0",
			"v1.6.0",
		},
		{
			"no-10-issue-2",
			"bazil.org/fuse",
			"v0.0.0-20160811212531-371fbbdaa898",
			"v0.0.0-20160811212531-371fbbdaa898",
		},
		{
			"no-10-issue-3",
			"github.com/disiqueira/gotree/v3",
			"v3.0.2",
			"v3.0.2",
		},
		{
			"no-10-issue-4",
			"github.com/willf/bitset",
			"v1.1.11",
			"v1.1.11",
		},
		{
			"no-10-issue-5",
			"github.com/coreos/etcd",
			"v3.3.12+incompatible",
			"v3.3.12+incompatible",
		},
		{
			"no-10-issue-7",
			"github.com/cncf/udpa/go",
			"v0.0.0-20191209042840-269d4d468f6f",
			"v0.0.0-20191209042840-269d4d468f6f",
		},
		{
			"no-10-issue-8",
			"dmitri.shuralyov.com/gpu/mtl",
			"v0.0.0-20190408044501-666a987793e9",
			"v0.0.0-20190408044501-666a987793e9",
		},
		{
			"no-9-issue-0",
			"github.com/moby/sys/mountinfo",
			"v0.4.0",
			"v0.4.0",
		},
		{
			"no-9-issue-1",
			"github.com/hashicorp/consul/api",
			"v1.1.0",
			"v1.1.0",
		},
		{
			"no-14-issue-0",
			"github.com/!microsoft/hcsshim",
			"v0.8.16",
			"v0.8.16",
		},
		{
			"no-14-issue-1",
			"github.com/!burnt!sushi/toml",
			"v0.3.1",
			"v0.3.1",
		},
	}

	var svc2 = NewDeferredModService("test", clients)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			info, err := svc2.GetVersion(context.TODO(), &module.Version{
				Path:    tt.moduleName,
				Version: tt.versionName,
			})
			assert.NoError(t, err)
			assert.EqualValues(t, tt.expected, info.Version)
		})
	}
}

//go:embed testdata/x-text-v0.3.6.mod
var xTextMod036 string

//go:embed testdata/x-text-hash.mod
var xTextModHash string

func TestModService_GetModuleInfo(t *testing.T) {
	var cases = []struct {
		name    string
		module  string
		version string
		mod     string
	}{
		{
			"explicit version resolves",
			"golang.org/x/text",
			"v0.3.6",
			xTextMod036,
		},
		{
			"implicit version resolves",
			"golang.org/x/text",
			"3115f89c4b99a620c7f1a4395a2b4405e95b82b6",
			xTextModHash,
		},
		{
			"missing info returns dummy mod",
			"golang.org/x/text",
			"v0.1.0",
			"module golang.org/x/text\n",
		},
		{
			"no-10-issue-5",
			"github.com/coreos/etcd",
			"v3.3.13+incompatible",
			"module github.com/coreos/etcd\n",
		},
		{
			"no-10-issue-6",
			"github.com/docker/docker",
			"v1.4.2-0.20191219165747-a9416c67da9f",
			"module github.com/docker/docker\n",
		},
		{
			"davecgh/go-spew resolves correctly",
			"github.com/davecgh/go-spew",
			"v1.1.1",
			"module github.com/davecgh/go-spew\n",
		},
	}

	var svc = NewDeferredModService("test", clients)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			var buf = &bytes.Buffer{}
			err := svc.GetModuleInfo(context.TODO(), buf, &module.Version{
				Path:    tt.module,
				Version: tt.version,
			})
			assert.NoError(t, err)
			assert.EqualValues(t, tt.mod, buf.String())
		})
	}
}

func TestModService_GetModuleInfo_missingTag(t *testing.T) {
	var svc = NewDeferredModService("test", clients)
	var buf = &bytes.Buffer{}
	err := svc.GetModuleInfo(context.TODO(), buf, &module.Version{
		Path:    "golang.org/x/text",
		Version: "v0.0.1",
	})
	assert.Error(t, err)
}

//go:embed testdata/x-tools-v0.1.4.mod
var xToolsMod string

//go:embed testdata/x-tools-gopls-v0.7.0.mod
var xToolsGoplsMod string

func TestModService_GetModuleInfo_subPath(t *testing.T) {
	var svc = NewDeferredModService("test", clients)
	var buf = &bytes.Buffer{}
	err := svc.GetModuleInfo(context.TODO(), buf, &module.Version{
		Path:    "golang.org/x/tools",
		Version: "v0.1.4",
	})
	assert.NoError(t, err)
	assert.EqualValues(t, xToolsMod, buf.String())

	buf = &bytes.Buffer{}
	err = svc.GetModuleInfo(context.TODO(), buf, &module.Version{
		Path:    "golang.org/x/tools/gopls",
		Version: "v0.7.0",
	})
	assert.NoError(t, err)
	assert.EqualValues(t, xToolsGoplsMod, buf.String())
}

func TestModService_GetModuleInfo_notFound(t *testing.T) {
	var svc = NewDeferredModService("test", clients)
	var buf = &bytes.Buffer{}
	err := svc.GetModuleInfo(context.TODO(), buf, &module.Version{
		Path:    "golang.org/x/tools/go/internal/cgo",
		Version: "latest",
	})
	assert.ErrorIs(t, err, ErrNotFound)
}

func TestModService_GetModuleInfo_integrity(t *testing.T) {
	var cases = []struct {
		name    string
		module  string
		version string
		hash    string
	}{
		{
			"github.com/gin-gonic/gin resolves correctly",
			"github.com/gin-gonic/gin",
			"v1.4.0",
			"h1:OW2EZn3DO8Ln9oIKOvM++LBO+5UPHJJDH72/q/3rZdM=",
		},
		{
			"pseudo version resolves",
			"gopkg.in/yaml.v3",
			"v3.0.0-20210107192922-496545a6307b",
			"h1:K4uyk7z7BCEPqu6E+C64Yfv1cQ7kz7rIZviUmN+EgEM=",
		},
		{
			"davecgh/go-spew resolves correctly",
			"github.com/davecgh/go-spew",
			"v1.1.1",
			"h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=",
		},
		{
			"github.com/!burnt!sushi/toml resolves correctly",
			"github.com/!burnt!sushi/toml",
			"v0.3.1",
			"h1:xHWCNGjB5oqiDr8zfno3MHue2Ht5sIBksp03qcyfWMU=",
		},
		{
			"github.com/ugorji/go/codec resolves correctly",
			"github.com/ugorji/go/codec",
			"v0.0.0-20181204163529-d75b2dcb6bc8",
			"h1:VFNgLljTbGfSG7qAOspJ7OScBnGdDN/yBr0sguwnwf0=",
		},
		{
			"github.com/coreos/etcd resolves correctly",
			"github.com/coreos/etcd",
			"v3.3.10+incompatible",
			"h1:uF7uidLiAD3TWHmW31ZFd/JWoc32PjwdhPthX9715RE=",
		},
		{
			"github.com/mediocregopher/radix/v3/@v/v3.3.0",
			"github.com/mediocregopher/radix/v3",
			"v3.3.0",
			"h1:EmfVyvspXz1uZEyPBMyGK+kjWiKQGvsUt6O3Pj+LDCQ=",
		},
	}

	var svc = NewDeferredModService("test", clients)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			file, err := os.Create(filepath.Join(t.TempDir(), "go.mod"))
			assert.NoError(t, err)
			err = svc.GetModuleInfo(context.TODO(), file, &module.Version{
				Path:    tt.module,
				Version: tt.version,
			})
			assert.NoError(t, err)
			assert.NoError(t, file.Close())

			// generate the hash of the mod file
			hash, err := checksum.HashGoMod(file.Name())
			assert.NoError(t, err)
			assert.EqualValues(t, tt.hash, hash.GoCheckSum)
		})
	}
}

func TestModService_GetModule(t *testing.T) {
	var cases = []struct {
		name    string
		module  string
		version string
		hash    string
	}{
		{
			"pseudo version resolves",
			"gopkg.in/yaml.v3",
			"v3.0.0-20210107192922-496545a6307b",
			"h1:h8qDotaEPuJATrMmW04NCwg7v22aHH28wwpauUhK9Oo=",
		},
		{
			"standard version resolves",
			"github.com/gin-gonic/gin",
			"v1.4.0",
			"h1:3tMoCCfM7ppqsR0ptz/wi1impNpT7/9wQtMZ8lr1mCQ=",
		},
		{
			"davecgh/go-spew resolves correctly",
			"github.com/davecgh/go-spew",
			"v1.1.1",
			"h1:vj9j/u1bqnvCEfJOwUhtlOARqs3+rkHYY13jYWTU97c=",
		},
		{
			"no-16-issue",
			"github.com/coreos/etcd",
			"v3.3.10+incompatible",
			"h1:jFneRYjIvLMLhDLCzuTuU4rSJUjRplcJQ7pD7MnhC04=",
		},
		{
			"no-16-issue",
			"github.com/kataras/iris/v12",
			"v12.0.1",
			"h1:Wo5S7GMWv5OAzJmvFTvss/C4TS1W0uo6LkDlSymT4rM=",
		},
		{
			"github.com/mediocregopher/radix/v3/@v/v3.3.0",
			"github.com/mediocregopher/radix/v3",
			"v3.3.0",
			"h1:oacPXPKHJg0hcngVVrdtTnfGJiS+PtwoQwTBZGFlV4k=",
		},
		{
			"github.com/labstack/echo/v4/@v/v4.1.11",
			"github.com/labstack/echo/v4",
			"v4.1.11",
			"h1:z0BZoArY4FqdpUEl+wlHp4hnr/oSR6MTmQmv8OHSoww=",
		},
	}

	var svc = NewDeferredModService("test", clients)
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			id := uuid.NewV4().String()
			file, err := os.Create(filepath.Join(t.TempDir(), "data.zip"))
			assert.NoError(t, err)
			// fetch the zip
			err = svc.GetModule(context.TODO(), file, &module.Version{
				Path:    tt.module,
				Version: tt.version,
			})
			assert.NoError(t, err)
			assert.NoError(t, file.Close())

			// unzip the data
			err = archiver.Unarchive(file.Name(), filepath.Join(os.TempDir(), id))
			assert.NoError(t, err)

			// generate the hash of the expanded directory
			slug := fmt.Sprintf("%s@%s", tt.module, tt.version)
			hash, err := checksum.HashDir(filepath.Join(os.TempDir(), id, slug), slug)
			assert.NoError(t, err)
			assert.EqualValues(t, tt.hash, hash.GoCheckSum)
		})
	}
}
