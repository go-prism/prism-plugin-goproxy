package api

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"go.opentelemetry.io/otel"
	"golang.org/x/mod/module"
	"io"
	"net/url"
	"strings"
)

type InterceptorModService struct {
	rulesClient api.RulesClient
	parent      BaseModService
}

func NewInterceptorModService(rulesClient api.RulesClient, p BaseModService) *InterceptorModService {
	svc := new(InterceptorModService)
	svc.parent = p
	svc.rulesClient = rulesClient

	return svc
}

func (svc *InterceptorModService) ListVersions(ctx context.Context, moduleName string) ([]*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_listVersions")
	defer span.End()
	name, ok := svc.rewriteModule(ctx, moduleName)
	if !ok {
		log.WithContext(ctx).Debugf("failed to rewrite module: %s", moduleName)
	}
	return svc.parent.ListVersions(ctx, name)
}

func (svc *InterceptorModService) GetVersion(ctx context.Context, version *module.Version) (*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_getVersion")
	defer span.End()
	name, ok := svc.rewriteModule(ctx, version.Path)
	if !ok {
		log.WithContext(ctx).Debugf("failed to rewrite module: %s", version.Path)
	}
	version.Path = name
	return svc.parent.GetVersion(ctx, version)
}

func (svc *InterceptorModService) GetModuleInfo(ctx context.Context, w io.Writer, version *module.Version) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_getModuleInfo")
	defer span.End()
	name, ok := svc.rewriteModule(ctx, version.Path)
	if !ok {
		log.WithContext(ctx).Debugf("failed to rewrite module: %s", version.Path)
	}
	version.Path = name
	return svc.parent.GetModuleInfo(ctx, w, version)
}

func (svc *InterceptorModService) GetModule(ctx context.Context, w io.Writer, version *module.Version) error {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_getModule")
	defer span.End()
	name, ok := svc.rewriteModule(ctx, version.Path)
	if !ok {
		log.WithContext(ctx).Debugf("failed to rewrite module: %s", version.Path)
	}
	version.Path = name
	return svc.parent.GetModule(ctx, w, version)
}

func (svc *InterceptorModService) GetLatestVersion(ctx context.Context, moduleName string) (*Info, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_getLatestVersion")
	defer span.End()
	name, ok := svc.rewriteModule(ctx, moduleName)
	if !ok {
		log.WithContext(ctx).Debugf("failed to rewrite module: %s", moduleName)
	}
	return svc.parent.GetLatestVersion(ctx, name)
}

func (svc *InterceptorModService) rewriteModule(ctx context.Context, name string) (string, bool) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "svc_interceptor_rewriteModule")
	defer span.End()
	// parse the name as an url.
	// add a scheme so Go can parse it properly
	uri, err := url.Parse(fmt.Sprintf("https://%s", name))
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to parse module as URL")
		return name, false
	}
	log.WithContext(ctx).Infof("fetching rules for host: %s", uri.Hostname())
	rule, err := svc.rulesClient.Get(utils.SubmitCtx(ctx), &api.GetRuleRequest{
		Source: uri.Hostname(),
	})
	if err != nil {
		span.RecordError(err)
		log.WithError(err).WithContext(ctx).Error("failed to retrieve matching rule")
		return name, false
	}
	log.WithContext(ctx).Debugf("successfully rewrote module %s to %s", rule.GetSource(), rule.GetDestination())
	uri.Host = rule.GetDestination()
	// return the uri and remove the scheme we
	// hacked in earlier
	return strings.ReplaceAll(uri.String(), "https://", ""), true
}
