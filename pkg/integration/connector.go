package integration

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	log "github.com/sirupsen/logrus"
	daov1 "gitlab.com/go-prism/prism-api/pkg/dto/v1"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"go.opentelemetry.io/otel"
)

const (
	NameRemote     = "GOPROXY"
	NameRefraction = "go1"
)

type Connector struct {
	remotes api.RemotesClient
	refract api.RefractionsClient
}

func NewConnector(remotes api.RemotesClient, refract api.RefractionsClient) *Connector {
	c := new(Connector)
	c.remotes = remotes
	c.refract = refract

	return c
}

// CreateLinks establishes the database objects required for
// the plugin to communicate with the Reactor.
func (c *Connector) CreateLinks(ctx context.Context) (string, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "connector_createLinks")
	defer span.End()
	remotes, err := c.refract.List(ctx, &empty.Empty{})
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to retrieve remotes")
		return "", err
	}
	var refract *v1.Refraction
	for _, r := range remotes.Refractions {
		if r.GetArchetype() == daov1.ArchetypeGo && r.GetName() == NameRefraction {
			if len(r.GetRemotes()) > 0 {
				log.WithContext(ctx).Infof("detected refraction with an established remote: %s", r.GetId())
				return r.GetName(), nil
			}
			log.WithContext(ctx).Infof("detected refraction: %s, however it is missing a remote", r.GetId())
			refract = r
			break
		}
	}
	// we're going to need a new remote
	remote, err := c.createRemote(ctx)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to create remote")
		return "", err
	}
	// create the refraction if we couldn't find one
	if refract == nil {
		log.WithContext(ctx).Warning("failed to locate refraction, we will create one")
		r, err := c.refract.Create(ctx, &api.CreateRefractRequest{
			Name:      NameRefraction,
			Archetype: daov1.ArchetypeGo,
			Remotes:   []string{remote.GetId()},
		})
		if err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to patch remote")
			return "", err
		}
		log.WithContext(ctx).Infof("created refraction with id: '%s'", r.GetId())
		return r.GetName(), nil
	}
	// patch the existing refraction to include the remote we just created
	log.WithContext(ctx).Infof("patching refraction to contain remote: '%s'", remote.GetId())
	if _, err := c.refract.Patch(ctx, &api.PatchRefractRequest{
		Id: refract.GetId(),
		Req: &api.CreateRefractRequest{
			Name:      refract.GetName(),
			Archetype: refract.GetArchetype(),
			Remotes:   []string{remote.GetId()},
		},
	}); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to patch remote")
		return "", err
	}
	return refract.GetName(), nil
}

// createRemote creates a dummy v1.Remote used by the
// GoProxy plugin.
func (c *Connector) createRemote(ctx context.Context) (*v1.Remote, error) {
	ctx, span := otel.Tracer(traceopts.DefaultTracerName).Start(ctx, "connector_createRemote")
	defer span.End()
	return c.remotes.Create(ctx, &api.CreateRemoteRequest{
		Name:              NameRemote,
		Uri:               "",
		Archetype:         daov1.ArchetypeGo,
		AllowList:         nil,
		BlockList:         nil,
		RestrictedHeaders: nil,
		StripRestricted:   false,
		Enabled:           true,
		ClientProfile:     nil,
	})
}
