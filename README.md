# Prism Plugin GoProxy

This plugin implements the [GOPROXY Protocol](https://golang.org/ref/mod#goproxy-protocol) and provides caching and offline support for GoLang module dependencies in the Prism application.

> This plugin is a work-in-progress and should not be used in production.

## The road so far

* [x] List info
* [ ] *(optional)* Latest info
* [x] Version info - basic support for pseudo-versions
* [x] Version mod
* [x] Version zip
* [x] Version caching
* [x] Metadata caching
* [x] Module caching

## Testing

Tests are executing using the standard GoLang toolchain.
Some tests will clone git repos and therefore require internet access.

```bash
go test ./...
```
