package testutils_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/testutils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

// interface guard
var _ api.ModulesClient = &testutils.TestModuleClient{}

func TestNewTestModuleClient(t *testing.T) {
	c := testutils.NewTestModuleClient()
	assert.NotNil(t, c)
}

func TestTestModuleClient(t *testing.T) {
	var mod = "golang.org/x/text"
	var v = "v0.3.6"
	c := testutils.NewTestModuleClient()
	version, err := c.Create(context.TODO(), &api.CreateModRequest{
		Name:      mod,
		Version:   v,
		Timestamp: "",
	})
	// ensure that creation works as expected
	assert.NoError(t, err)
	assert.EqualValues(t, mod, version.ModuleName)
	assert.EqualValues(t, v, version.VersionName)
	assert.Empty(t, version.Timestamp)

	version, err = c.Get(context.TODO(), &api.GetModRequest{
		Name:    mod,
		Version: v,
	})
	// ensure that we can retrieve the created module
	assert.NoError(t, err)
	assert.EqualValues(t, mod, version.ModuleName)
	assert.EqualValues(t, v, version.VersionName)
	assert.Empty(t, version.Timestamp)

	versions, err := c.List(context.TODO(), &api.ListModRequest{
		Name: mod,
	})
	assert.NoError(t, err)
	assert.Len(t, versions.Versions, 1)
}

func TestTestModuleClient_Get(t *testing.T) {
	c := testutils.NewTestModuleClient()
	v, err := c.Get(context.TODO(), &api.GetModRequest{
		Name:    "",
		Version: "",
	})
	assert.Nil(t, v)
	assert.Error(t, err)
	assert.EqualValues(t, codes.NotFound, status.Code(err))
}
