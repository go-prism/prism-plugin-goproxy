package testutils

import (
	"context"
	"errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc"
)

type TestRulesClient struct {
	api.RulesClient
}

func (c *TestRulesClient) Get(_ context.Context, in *api.GetRuleRequest, _ ...grpc.CallOption) (*archv1.HostRewrite, error) {
	if in.GetSource() == "github.com" {
		return &archv1.HostRewrite{
			Id:          uuid.NewV4().String(),
			Source:      "github.com",
			Destination: "git.example.org",
		}, nil
	}
	return nil, errors.New("some error")
}
