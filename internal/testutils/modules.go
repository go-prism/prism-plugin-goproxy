package testutils

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/go-prism/prism-rpc/domain/archv1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TestModuleClient struct {
	api.ModulesClient
	data []*archv1.ModuleVersion
}

func NewTestModuleClient() *TestModuleClient {
	c := new(TestModuleClient)
	c.data = []*archv1.ModuleVersion{}

	return c
}

func (c *TestModuleClient) List(_ context.Context, req *api.ListModRequest, _ ...grpc.CallOption) (*archv1.ModuleVersions, error) {
	var versions []*archv1.ModuleVersion
	for _, v := range c.data {
		if v.ModuleName == req.Name {
			versions = append(versions, v)
		}
	}
	return &archv1.ModuleVersions{
		Versions: versions,
	}, nil
}

func (c *TestModuleClient) Get(_ context.Context, req *api.GetModRequest, _ ...grpc.CallOption) (*archv1.ModuleVersion, error) {
	for _, v := range c.data {
		if v.ModuleName == req.Name && v.VersionName == req.Version {
			return v, nil
		}
	}
	return nil, status.Error(codes.NotFound, "not found")
}

func (c *TestModuleClient) Create(_ context.Context, req *api.CreateModRequest, _ ...grpc.CallOption) (*archv1.ModuleVersion, error) {
	v := &archv1.ModuleVersion{
		Id:          uuid.NewV4().String(),
		ModuleName:  req.Name,
		VersionName: req.Version,
		Timestamp:   req.Timestamp,
	}
	c.data = append(c.data, v)
	return v, nil
}
