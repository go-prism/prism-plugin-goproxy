package testutils

import (
	"context"
	"errors"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc"
)

type TestReactorClient struct {
	reactor.ReactorClient
}

func (c *TestReactorClient) GetStatus(context.Context, *empty.Empty, ...grpc.CallOption) (*reactor.ReactorStatus, error) {
	return nil, errors.New("not implemented")
}
func (c *TestReactorClient) GetObject(context.Context, *reactor.GetObjectRequest, ...grpc.CallOption) (*reactor.GetObjectResponse, error) {
	return nil, errors.New("not implemented")
}
func (c *TestReactorClient) GetObjectV2(context.Context, *reactor.GetObjectRequest, ...grpc.CallOption) (reactor.Reactor_GetObjectV2Client, error) {
	return nil, errors.New("not implemented")
}
func (c *TestReactorClient) GetRefractInfo(context.Context, *reactor.GetRefractInfoRequest, ...grpc.CallOption) (*reactor.GetRefractInfoResponse, error) {
	return nil, errors.New("not implemented")
}
func (c *TestReactorClient) PutObjectV2(context.Context, ...grpc.CallOption) (reactor.Reactor_PutObjectV2Client, error) {
	return nil, errors.New("not implemented")
}
func (c *TestReactorClient) InvalidateControlRod(context.Context, *reactor.InvalidateControlRodRequest, ...grpc.CallOption) (*empty.Empty, error) {
	return nil, errors.New("not implemented")
}
