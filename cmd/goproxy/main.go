package main

import (
	"context"
	"fmt"
	"gitlab.com/av1o/cap10-ingress/pkg/tracing"
	"gitlab.com/go-prism/prism-plugin-goproxy/internal/traceopts"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/djcass44/go-tracer/tracer"
	"gitlab.com/go-prism/prism-plugin-goproxy/pkg/api"
	"gitlab.com/go-prism/prism-plugin-goproxy/pkg/integration"
	"gitlab.com/go-prism/prism-rpc/pkg/rpclient"
	api2 "gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc"

	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/av1o/cap10-ingress/pkg/logging"
	"gitlab.com/go-prism/prism-api/pkg/flag"
	"gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/metrics"
)

type environment struct {
	Port      int `default:"8080"`
	Log       logging.Config
	Flag      flag.Config
	ClientTLS httputils.TLSConfig `split_words:"true"`

	ReactorURL  string `default:"localhost:8081" split_words:"true"`
	ApiURL      string `default:"localhost:8080" split_words:"true"`
	Environment string `split_words:"true" envconfig:"GITLAB_ENVIRONMENT_NAME"`
}

// @title Prism GoProxy Plugin
// @description Component of Prism that implements the GOPROXY mechanism.
// @version 0.1

// @license.name Apache 2.0
// @license.url https://www.apache.org/licenses/LICENSE-2.0
func main() {
	// read config
	var e environment
	err := envconfig.Process("prism", &e)
	if err != nil {
		log.WithError(err).Fatal("failed to read environment")
		return
	}

	// configure logging
	logging.Init(&e.Log)
	log.Debugf("starting with config: %+v", e)

	// setup otel
	if err := tracing.Init(tracing.OtelOptions{
		ServiceName: traceopts.DefaultServiceName,
		Environment: e.Environment,
	}); err != nil {
		log.WithError(err).Fatal("failed to setup tracing")
		return
	}

	// configure http
	router := mux.NewRouter()
	metrics.Init(router, "/metrics")
	// enable k8s probes
	router.HandleFunc("/ping", httputils.PingFunc)

	// enable http tracing
	router.Use(tracer.NewHandler)
	router.Use(otelmux.Middleware(traceopts.DefaultServiceName))

	// setup clients
	sc, err := rpclient.NewAutoConn(e.ReactorURL, e.ClientTLS.CertFile, e.ClientTLS.KeyFile, e.ClientTLS.CaFile,
		grpc.WithStreamInterceptor(otelgrpc.StreamClientInterceptor()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
	)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to reactor")
		return
	}
	apiSc, err := grpc.Dial(e.ApiURL, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Fatal("failed to connect to api")
		return
	}
	reactorClient := reactor.NewReactorClient(sc)
	remotesClient := api2.NewRemotesClient(apiSc)
	refractClient := api2.NewRefractionsClient(apiSc)
	moduleClient := api2.NewModulesClient(apiSc)
	rulesClient := api2.NewRulesClient(apiSc)

	// setup api links
	refractionName, err := integration.NewConnector(remotesClient, refractClient).CreateLinks(context.TODO())
	if err != nil {
		log.WithError(err).Fatal("failed to setup API links")
		return
	}

	// setup routes
	_ = api.NewGoModules(api.NewInterceptorModService(rulesClient, api.NewDeferredModService(refractionName, &api.Clients{
		ReactorClient: reactorClient,
		ModulesClient: moduleClient,
	})), router)

	// start http server
	go func() {
		addr := fmt.Sprintf(":%d", e.Port)
		log.WithField("addr", addr).Infof("starting server on interface %s", addr)
		log.Fatal(http.ListenAndServe(addr, router))
	}()

	// wait for a signal
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	<-sigC
	log.Info("received SIGTERM/SIGINT, shutting down...")
}
