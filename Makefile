.PHONY: run
run:
	skaffold run -f ./deployments/skaffold.yaml

.PHONY: dev
dev:
	skaffold dev -f ./deployments/skaffold.yaml

.PHONY: test
test:
	scripts/test.sh