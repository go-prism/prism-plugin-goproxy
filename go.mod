module gitlab.com/go-prism/prism-plugin-goproxy

go 1.16

replace go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp => github.com/djcass44/opentelemetry-go-contrib/instrumentation/net/http/otelhttp v0.24.1-0.20211004101437-3f58f5c72838

require (
	github.com/djcass44/go-tracer v0.3.0
	github.com/djcass44/go-utils v0.2.1-0.20210620031251-55865f8afe92
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/mux v1.8.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mholt/archiver/v3 v3.5.0
	github.com/prometheus/client_golang v1.11.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/vikyd/go-checksum v1.0.0
	gitlab.com/av1o/cap10-ingress v0.0.0-20211004102052-6c306be11c0f
	gitlab.com/go-prism/prism-api v0.0.0-20210801111126-f2ec2177197a
	gitlab.com/go-prism/prism-rpc v0.0.0-20211005080206-16041e0e244d
	go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux v0.24.0
	go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc v0.24.0
	go.opentelemetry.io/otel v1.0.1
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/mod v0.5.0
	google.golang.org/grpc v1.40.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
